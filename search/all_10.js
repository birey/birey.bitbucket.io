var searchData=
[
  ['s0_5finit_103',['S0_INIT',['../classtermproject_1_1Balance.html#a892ce441cccbf1853bdd27a9c8f172a9',1,'termproject::Balance']]],
  ['s1_5fwait_104',['S1_WAIT',['../classtermproject_1_1Balance.html#a9f1ac7b2c76f249e736a4844aaee61b8',1,'termproject::Balance']]],
  ['s2_5fon_105',['S2_ON',['../classtermproject_1_1Balance.html#a14aa37c8d1148cff0f17075fb4728e9d',1,'termproject::Balance']]],
  ['s3_5foff_106',['S3_OFF',['../classtermproject_1_1Balance.html#ac5f1c744faea2f3d29ff39bec9d224a0',1,'termproject::Balance']]],
  ['sample_107',['sample',['../classtouch_1_1TCH.html#ae30154cd1855199babb99eb8b5997d54',1,'touch::TCH']]],
  ['sample_5fset_108',['sample_set',['../classtouch_1_1TCH.html#a1f15669b4e367b5c289f6e593ea34d2a',1,'touch::TCH']]],
  ['sensor_5fdata_109',['sensor_data',['../Lab4__main_8py.html#a6f03577d485cef3ca626950ff41a2ad6',1,'Lab4_main']]],
  ['sensor_5ffunction_110',['sensor_function',['../Lab4__main_8py.html#abc707a900e8d20c971f8b11fa0c05666',1,'Lab4_main']]],
  ['set_5fposition_111',['set_position',['../classEncoder_1_1Raw__Encoder.html#a95bbc3b221d532940741d8e438109982',1,'Encoder.Raw_Encoder.set_position()'],['../classEncoder_1_1Encoder.html#a7afd1019eef54a1597bf52695e7a9bf0',1,'Encoder.Encoder.set_position()']]],
  ['slave_5faddress_112',['slave_address',['../Lab4__main_8py.html#a7bfd878d0efdaec622abf2498050cfe9',1,'Lab4_main']]],
  ['sleep_113',['sleep',['../classMotorDriver_1_1MotorDriver.html#a7f488e8a8da0beb4819717428b9c00f0',1,'MotorDriver::MotorDriver']]],
  ['state_114',['state',['../classEncoder_1_1Encoder.html#ab682cdb03d2a5a4a6da43d3ed64c348c',1,'Encoder.Encoder.state()'],['../classEncoder_1_1Encoder__Interface.html#a550d9374dc04a1c112121402d6bec7bd',1,'Encoder.Encoder_Interface.state()'],['../classtermproject_1_1Balance.html#a2e9393b9fb318cd28ff85e0cb9f57ca8',1,'termproject.Balance.state()']]]
];
