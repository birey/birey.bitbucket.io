var classLab__07__ER__TFT080__1__Driver_1_1ER__TFT080__1 =
[
    [ "__init__", "classLab__07__ER__TFT080__1__Driver_1_1ER__TFT080__1.html#a5c79a1dfe2ae3a215ddb2a28a964f48d", null ],
    [ "all_scan", "classLab__07__ER__TFT080__1__Driver_1_1ER__TFT080__1.html#a39f1ec5496bd79d9a5fae2dcfa7c19a2", null ],
    [ "x_scan", "classLab__07__ER__TFT080__1__Driver_1_1ER__TFT080__1.html#abee72f8d3f60cffb82b0bcf691feed0f", null ],
    [ "y_scan", "classLab__07__ER__TFT080__1__Driver_1_1ER__TFT080__1.html#a39d588a9286d21520b72ed2026d0afba", null ],
    [ "z_scan", "classLab__07__ER__TFT080__1__Driver_1_1ER__TFT080__1.html#ad55304d833eaf7ceff2a9e1a024a74d7", null ],
    [ "pin1", "classLab__07__ER__TFT080__1__Driver_1_1ER__TFT080__1.html#ad7fd88c9b525d5e25eefe4c73e48bc94", null ],
    [ "pin2", "classLab__07__ER__TFT080__1__Driver_1_1ER__TFT080__1.html#a2bec65db1a28af4f8e325cce078e90e2", null ],
    [ "pin3", "classLab__07__ER__TFT080__1__Driver_1_1ER__TFT080__1.html#ac99048a28e1f4b0608d6532518cc5b04", null ],
    [ "pin4", "classLab__07__ER__TFT080__1__Driver_1_1ER__TFT080__1.html#a0c28b15ba18fcdbf3958ca7d94e618a0", null ],
    [ "pin_xm", "classLab__07__ER__TFT080__1__Driver_1_1ER__TFT080__1.html#a746ed545f0694e3ea76e7c6f2c9e0da2", null ],
    [ "pin_xp", "classLab__07__ER__TFT080__1__Driver_1_1ER__TFT080__1.html#a96f34c8031617b42060e060e189cc693", null ],
    [ "pin_ym", "classLab__07__ER__TFT080__1__Driver_1_1ER__TFT080__1.html#aa46c5d8a9b7abd53e555914f2f25db42", null ],
    [ "pin_yp", "classLab__07__ER__TFT080__1__Driver_1_1ER__TFT080__1.html#a57f0deb63576f37fede3347a61d0c845", null ]
];