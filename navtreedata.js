/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "ME 405 Documentation Portfolio", "index.html", [
    [ "Introduction", "index.html#sec_intro", null ],
    [ "Lab 01", "page_lab01.html", [
      [ "Source Code Access", "page_lab01.html#page_lab01_src", null ],
      [ "Documentation", "page_lab01.html#page_lab01_doc", null ]
    ] ],
    [ "Lab 02", "page_lab02.html", [
      [ "Source Code Access", "page_lab02.html#page_lab02_src", null ],
      [ "Documentation", "page_lab02.html#page_lab02_doc", null ]
    ] ],
    [ "Lab 03", "page_lab03.html", [
      [ "Source Code Access", "page_lab03.html#page_lab03_src", null ],
      [ "Documentation", "page_lab03.html#page_lab03_doc", null ]
    ] ],
    [ "Lab 04", "page_lab04.html", [
      [ "Source Code Access", "page_lab04.html#page_lab04_src", null ],
      [ "Documentation", "page_lab04.html#page_lab04_doc", null ],
      [ "Data", "page_lab04.html#page_lab04_data", null ]
    ] ],
    [ "Lab 05", "page_lab05.html", [
      [ "Description", "page_lab05.html#page_lab05_description", null ],
      [ "Calculations", "page_lab05.html#page_lab05_calcs", null ]
    ] ],
    [ "Lab 06", "page_lab06.html", [
      [ "Description", "page_lab06.html#page_lab06_description", null ]
    ] ],
    [ "Lab 07", "page_lab07.html", [
      [ "Source Code Access", "page_lab07.html#page_lab07_src", null ],
      [ "Documentation", "page_lab07.html#page_lab07_doc", null ]
    ] ],
    [ "Lab 08", "page_lab08.html", [
      [ "Source Code Access", "page_lab08.html#page_lab08_src", null ],
      [ "Documentation", "page_lab08.html#page_lab08_doc", null ]
    ] ],
    [ "Lab 09", "page_lab09.html", [
      [ "Source Code Access", "page_lab09.html#page_lab09_src", null ],
      [ "Documentation", "page_lab09.html#page_lab09_doc", null ]
    ] ],
    [ "Packages", "namespaces.html", [
      [ "Packages", "namespaces.html", "namespaces_dup" ],
      [ "Package Functions", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Variables", "namespacemembers_vars.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"",
"page_lab08.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';